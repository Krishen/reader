﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reader
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


       public Bitmap bmp, raw_bmp,new_bmp;
        private TracingsForm tf;

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {

            this.points = new List<Point>();
            
            this.openFileDialog.ShowDialog();

            raw_bmp = new Bitmap(this.openFileDialog.FileName);


            for (int x = 0; x < raw_bmp.Width; ++x)
            {
                for (int y = 0; y < raw_bmp.Height; ++y)
                {
                    Color color = raw_bmp.GetPixel(x, y);
                    byte h = Convert.ToByte((color.R + color.G + color.B) / 3);
                    Color gray = Color.FromArgb(h, h, h);
                    raw_bmp.SetPixel(x, y, gray);
                }
            }

            tf = new TracingsForm(this);
            tf.Width = 1000;
            tf.Height = 700;


            bmp = new Bitmap(raw_bmp, new Size(this.tf.pictureBox.Width, this.tf.pictureBox.Height));

     
            tf.ShowDialog();




          //  this.tf.pictureBox.Image = raw_bmp;


        }


        public List<Point> points;

 

        private double get_S(Point p0, Point p1, Point p2)
        {
            double a = Math.Sqrt((p1.X - p0.X) * (p1.X - p0.X) + (p1.Y - p0.Y) * (p1.Y - p0.Y));
            double b = Math.Sqrt((p2.X - p1.X) * (p2.X - p1.X) + (p2.Y - p1.Y) * (p2.Y - p1.Y));
            double с = Math.Sqrt((p0.X - p2.X) * (p0.X - p2.X) + (p0.Y - p2.Y) * (p0.Y - p2.Y));

            double p = (a + b + с) / 2.0;

            double s = Math.Sqrt(p * (p - a) * (p - b) * (p - с));

            return s;
        }

        public void barycentric()
        {
            new_bmp = new Bitmap(1500,1000);

            Point p0 = new Point(0, 0);
            Point p1 = new Point(new_bmp.Width, 0);
            Point p2 = new Point(new_bmp.Width, new_bmp.Height);
            Point p3 = new Point(0, new_bmp.Height);

            double S = get_S(p0, p1, p3);
            
                       for (int x = 0; x < new_bmp.Width; ++x)
                       {
                           for (int y = 0; y < new_bmp.Height; ++y)
                           {

                               try
                               {
                                   double s1 = get_S(p1, p3, new Point(x, y)) / S;
                                   double s2 = get_S(p0, p3, new Point(x, y)) / S;
                                   double s3 = get_S(p0, p1, new Point(x, y)) / S;

                                   if (s1 + s2 + s3 > 1.001) continue;

                                   double kw = raw_bmp.Width / (double)(bmp.Width);
                                   double kh = raw_bmp.Height / (double)(bmp.Height);

                                   Color col = raw_bmp.GetPixel((int)(points[0].X * kw * s1 + points[1].X * kw * s2 + points[3].X * s3 * kw), (int)(points[0].Y * kh * s1 + points[1].Y * kh * s2 + points[3].Y * s3 * kh));

                                   new_bmp.SetPixel(x, y, col);
                               }
                               catch { }
                            }
                       }
                        
            for (int x = 0; x < new_bmp.Width; ++x)
           {
               for (int y = 0; y < new_bmp.Height; ++y)
               {

                   try
                   {
                       double s1 = get_S(p2, p3, new Point(x, y)) / S;
                       double s2 = get_S(p3, p1, new Point(x, y)) / S;
                       double s3 = get_S(p1, p2, new Point(x, y)) / S;

                       if (s1 + s2 + s3 > 1.001) continue;

                       double kw = raw_bmp.Width / (double)(bmp.Width);
                       double kh = raw_bmp.Height / (double)(bmp.Height);

                       Color col = raw_bmp.GetPixel((int)(points[1].X * kw * s1 + points[2].X * kw * s2 + points[3].X * s3 * kw), (int)(points[1].Y * kh * s1 + points[2].Y * kh * s2 + points[3].Y * s3 * kh));

                       new_bmp.SetPixel(x, y, col);
                   }
                   catch { }
               }
           }


            b = new List<Bitmap>();

           this.pictureBox2.Image = new_bmp.Clone(new Rectangle(0,0,500,500),new_bmp.PixelFormat);
           b.Add(new_bmp.Clone(new Rectangle(0,0,500,500),new_bmp.PixelFormat));

           this.pictureBox3.Image = new_bmp.Clone(new Rectangle(500, 0, 500, 500), new_bmp.PixelFormat);
           b.Add( new_bmp.Clone(new Rectangle(500, 0, 500, 500), new_bmp.PixelFormat));

           this.pictureBox4.Image = new_bmp.Clone(new Rectangle(1000, 0, 500, 500), new_bmp.PixelFormat);
           b.Add(new_bmp.Clone(new Rectangle(1000, 0, 500, 500), new_bmp.PixelFormat));

           this.pictureBox5.Image = new_bmp.Clone(new Rectangle(0, 500, 500, 500), new_bmp.PixelFormat);
           b.Add(new_bmp.Clone(new Rectangle(0, 500, 500, 500), new_bmp.PixelFormat));

           this.pictureBox6.Image = new_bmp.Clone(new Rectangle(500, 500, 500, 500), new_bmp.PixelFormat);
           b.Add(new_bmp.Clone(new Rectangle(500, 500, 500, 500), new_bmp.PixelFormat));

           this.pictureBox7.Image = new_bmp.Clone(new Rectangle(1000, 500, 500, 500), new_bmp.PixelFormat);
           b.Add(new_bmp.Clone(new Rectangle(1000, 500, 500, 500), new_bmp.PixelFormat));

           new_bmp.Dispose();
           return;
        }

        List<Bitmap> b;

        private void pictureBox_Click(object sender, EventArgs e)
        {


        }

        public Point point=new Point(0,0);

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            
        }

        private Bitmap pattern;


       int[,] sum_w , sum_b ;

       public void f()
        {

            sum_b = new int[6,5];
            sum_w = new int[6,5];

            Bitmap tmp_pat = (Bitmap)pattern.Clone();

          
            for (int x = 0; x < pattern.Width; x++)
            {
                for (int y = 0; y < pattern.Height; y++)
                {

                    Color c1=pattern.GetPixel(x,y);

                    int l = 0;

                 //   if (y > 50) l++;
                    if (x > 100) l++;
               //     if (y > 150) l++;
                    if (x > 200) l++;
             //       if (y > 250) l++;
                    if (x > 300) l++;
             //       if (y > 350) l++;
                    if (x > 400) l++;
             //       if (y > 450) l++;

                    for (int k = 0; k < 6; k++)
                    {

                        Color c2 = b[k].GetPixel(x, y);

                        if (c1.R > 250)
                        {
                            sum_w[k,l] += c2.R;
                        }
                        else
                        {
                            sum_b[k,l] += c2.R;
                        }
                    }

        

                }
            }

           

            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();
            listBox6.Items.Clear();

            for (int i = 0; i < 5; i++)
            {
                listBox1.Items.Add(Math.Abs(sum_w[0,i] - sum_b[0,i]).ToString());
                listBox2.Items.Add(Math.Abs(sum_w[1,i] - sum_b[1,i]).ToString());
                listBox3.Items.Add(Math.Abs(sum_w[2,i] - sum_b[2,i]).ToString());
                listBox4.Items.Add(Math.Abs(sum_w[3,i] - sum_b[3,i]).ToString());
                listBox5.Items.Add(Math.Abs(sum_w[4,i] - sum_b[4,i]).ToString());
                listBox6.Items.Add(Math.Abs(sum_w[5, i] - sum_b[5,i]).ToString());
            }

            listBox1.Items.Add("");
            listBox2.Items.Add("");
            listBox3.Items.Add("");
            listBox4.Items.Add("");
            listBox5.Items.Add("");
            listBox6.Items.Add("");

            int[] summ = new int[6];

            for (int i = 0; i < 6; i++)
            {
                summ[i] = 0;
            }


            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    summ[i] += Math.Abs(sum_w[i, j] - sum_b[i, j]);
                }
            }

            listBox1.Items.Add(summ[0]);
            listBox2.Items.Add(summ[1]);
            listBox3.Items.Add(summ[2]);
            listBox4.Items.Add(summ[3]);
            listBox5.Items.Add(summ[4]);
            listBox6.Items.Add(summ[5]);


            return;
        }


        private void MainForm_Load(object sender, EventArgs e)
        {
            this.points = new List<Point>();
            this.pattern = new Bitmap("pattern.bmp");
        }

       
    }
}
