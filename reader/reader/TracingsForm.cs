﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reader
{
    public partial class TracingsForm : Form
    {
        MainForm mf;
        public TracingsForm(MainForm mf)
        {
            InitializeComponent();
            this.mf = mf;
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {

            try
            {
                if (this.mf.points.Count < 4)
                {

                    this.mf.points.Add(this.mf.point);


                    this.draw();
                }
            }

            catch { }
        }

        private void pictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                this.mf.point.X = e.X;
                this.mf.point.Y = e.Y;


                if (mf.points.Count < 4)
                {

                    this.draw();
                 
                }
                else
                {
                    this.Close();
                }   
            }
            catch { }
        }

        private void TracingsForm_Load(object sender, EventArgs e)
        {
            this.pictureBox.Image = mf.bmp;

        }



        public void draw()
        {

            this.pictureBox.Image = (Bitmap)mf.bmp.Clone();

            Graphics g = Graphics.FromImage(this.pictureBox.Image);

          
            if (mf.points.Count > 0 && mf.points.Count < 4)
            {
                g.DrawLine(new Pen(Color.Red, 3.0f), mf.point, mf.points[mf.points.Count - 1]);
            }



            for (int i = 0; i < mf.points.Count - 1; i++)
            {
                g.DrawLine(new Pen(Color.Green, 3.0f), mf.points[i], mf.points[i + 1]);

            }
            if (mf.points.Count == 4)
            {

                g.DrawLine(new Pen(Color.Green, 3.0f), mf.points[0], mf.points[3]);

                mf.barycentric();

                mf.f();

            }
        }


    }
}
