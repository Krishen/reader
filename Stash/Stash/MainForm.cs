﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace Stash
{
    public partial class MainForm : Form
    {
        private static string TABLECODING = "Table coding.xml";
        private int _numberMaskLine = 5;
        private int _colorStep = 60;
       // private int _divider = 5;
        private int _rows = 2;
        private int _columns = 3;

        private Bitmap _bmp;
        private Bitmap _source;
        private Dictionary<char, byte> _tableCoding = new Dictionary<char, byte>();

        public MainForm()
        {
            InitializeComponent();
            getCharacters();
        }

        private void getCharacters()
        {          
            XmlDocument doc = new XmlDocument();
            doc.Load(TABLECODING);
            foreach(XmlNode n in doc.ChildNodes[1].ChildNodes)
            {
                _tableCoding.Add(n.InnerText[0], Convert.ToByte(n.Attributes["code"].Value));
                characterBox.Items.Add(n.InnerText[0]);
            }
        }

        private void buttonOpenFile_Click(object sender, EventArgs e)
        {
            oFD.Filter = "Image Files(*.BMP;*.JPG;*.GIF:*.PNG)|*.BMP;*.JPG;*.GIF;*.PNG";
            oFD.RestoreDirectory = true;
            if (oFD.ShowDialog() == DialogResult.OK)
            {
                _source = new Bitmap(oFD.FileName);

                toGray();

                pictureBox1.Image = _source;
               // buttonSave.Enabled = false;
            }
        }

        private void buttonAddChar_Click(object sender, EventArgs e)
        {
            if (characterBox.SelectedIndex != -1 && _source != null)
            {
                _bmp = (Bitmap)_source.Clone();

                int width = _bmp.Width;
                int height = _bmp.Height;
                int w_line = width / _columns / _numberMaskLine / 2;// / _divider;
                int offset = w_line;
                int h_line = height / _rows;

                byte b = _tableCoding[characterBox.SelectedItem.ToString()[0]];
                string mask = Convert.ToInt32(Convert.ToString(b, 2)).ToString("000000");
                
                for (int h=0; h<_rows; ++h)
                {
                    for (int w=0, x=0; w<_columns;  ++w, x += width / _columns)
                    {
                        if (mask[h * _columns + w] == '1')
                        {
                            int cursor = x;
                            for (int line = 0; line < _numberMaskLine; ++line)
                            {
                                cursor += offset;
                                for (int i = 0; i < w_line; ++i, ++cursor)
                                {
                                    for (int y = h * height / _rows; y < (h + 1) * height / _rows; ++y)
                                    {
                                        Color back = _bmp.GetPixel(cursor, y);
                                        byte c = Convert.ToByte( back.R < _colorStep ? back.R + _colorStep : back.R - _colorStep );
                                        Color colorLine = Color.FromArgb(c, c, c);
                                        _bmp.SetPixel(cursor, y, colorLine);
                                    }
                                }
                            }
                        }
                    }
                }

                pictureBox1.Image = _bmp;
                buttonSave.Enabled = true;
            }
        }

        private void toGray()
        {
            for(int x = 0; x < _source.Width; ++x)
            {
                for(int y = 0; y < _source.Height; ++y)
                {
                    Color color = _source.GetPixel(x, y);
                    byte h = Convert.ToByte((color.R + color.G + color.B) / 3);
                    Color gray = Color.FromArgb(h, h, h);
                    _source.SetPixel(x, y, gray);
                }
            }
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            sFD.Filter = "(*.bmp)|*.bmp";
            sFD.RestoreDirectory = true;
            if (sFD.ShowDialog() == DialogResult.OK)
            {
                _bmp.Save(sFD.FileName);
            }
        }

    }
}
